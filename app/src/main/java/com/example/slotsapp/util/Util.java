package com.example.slotsapp.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.slotsapp.R;

public class Util {

    public static Bitmap svgToBitmap(Drawable vectorDrawable, int x, int y) {
        Bitmap bitmap = Bitmap.createBitmap(x,
                y, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        vectorDrawable.draw(canvas);
        return bitmap;
    }

    public static void imageViewAnimatedChange(Context context, ImageView v, ConstraintLayout parent, Bitmap new_image) {
        if(v.getDrawable() == null) {
            v.setImageBitmap(new_image);
        } else {
            ImageView view = new ImageView(context);
            view.setImageBitmap(((BitmapDrawable)v.getDrawable()).getBitmap());
            parent.addView(view, v.getLayoutParams());

            if(v.getAnimation() != null) {
                v.setAnimation(null);
            }

            Animation anim_out = AnimationUtils.loadAnimation(context, R.anim.anim_out_top);
            Animation anim_in  = AnimationUtils.loadAnimation(context, R.anim.anim_in_bottom);

            anim_in.setAnimationListener(new Animation.AnimationListener() {
                @Override public void onAnimationStart(Animation animation) { }
                @Override public void onAnimationEnd(Animation animation) {
                    v.requestLayout();
                }
                @Override public void onAnimationRepeat(Animation animation) { }
            });

            anim_out.setAnimationListener(new Animation.AnimationListener() {
                @Override public void onAnimationStart(Animation animation) { }
                @Override public void onAnimationEnd(Animation animation) {
                    parent.removeView(view);
                }
                @Override public void onAnimationRepeat(Animation animation) { }
            });

            v.startAnimation(anim_in);
            v.setImageBitmap(new_image);
            view.setAnimation(anim_out);
        }
    }
}
