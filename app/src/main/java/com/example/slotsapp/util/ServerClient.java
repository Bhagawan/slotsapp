package com.example.slotsapp.util;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ServerClient {

    @FormUrlEncoded
    @POST("SlotsApp/splash.php")
    Call<SplashResponse> getSplash(@Field("locale") String locale);

}
