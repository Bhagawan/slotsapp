package com.example.slotsapp.mvp;

import android.graphics.Bitmap;

import moxy.MvpView;
import moxy.viewstate.strategy.alias.OneExecution;

public interface SlotPresenterViewInterface extends MvpView {

    @OneExecution
    void setSlot(Bitmap image, int num);

    @OneExecution
    void showMessage(String message);

    @OneExecution
    void changeScore(int score);

}
