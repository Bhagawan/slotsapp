package com.example.slotsapp.mvp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.VectorDrawable;
import android.os.Handler;
import android.os.Looper;

import androidx.core.content.ContextCompat;

import com.example.slotsapp.R;
import com.example.slotsapp.util.Util;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import moxy.InjectViewState;
import moxy.MvpPresenter;

@InjectViewState
public class SlotPresenter extends MvpPresenter<SlotPresenterViewInterface> {
    private final ArrayList<Bitmap> images = new ArrayList<>();
    private ExecutorService executor = Executors.newSingleThreadExecutor();
    private Handler handler = new Handler(Looper.getMainLooper());

    Context context;

    private int score = 300;
    private boolean spinning = false;
    private byte streak = 0;

    public void initialise(Context context) {
        this.context = context;
        images.add(Util.svgToBitmap( ContextCompat.getDrawable(context, R.drawable.ic_bomb)
                , dpToPx(100), dpToPx(75)));
        images.add(Util.svgToBitmap( ContextCompat.getDrawable(context, R.drawable.ic__641998142bow_and_arrow_clip_art)
                ,dpToPx(100), dpToPx(100)));
        images.add(Util.svgToBitmap( ContextCompat.getDrawable(context, R.drawable.ic_stern)
                ,dpToPx(100), dpToPx(100)));
        getViewState().changeScore(score);
        handler.post(() -> setPicture(0, 1));
        handler.post(() -> setPicture(0, 2));
        handler.post(() -> setPicture(0, 3));
    }

    public void spin() {
        if(score >= 50 && !spinning) {
            score -= 50;
            showScore();
            spinning = true;
            Random r = new Random();
            executor.execute(() -> {
                int n = 0;
                int finalFirst = -1;
                int finalSecond = -1;
                while(n < 7) {
                    try {
                        int first = r.nextInt(images.size());
                        int second = r.nextInt(images.size());
                        int third = r.nextInt(images.size());
                        if(n < 5) {
                            handler.post(() -> setPicture(first, 1));
                        }
                        if(n > 0 && n < 6) {
                            handler.post(() -> setPicture(second, 2));
                        }
                        if(n > 1) {
                            handler.post(() -> setPicture(third, 3));
                        }
                        if(n == 4) finalFirst = first;
                        if(n == 5) finalSecond = second;
                        n++;
                        if(n == 7) {
                            endSpin(finalFirst == finalSecond && finalSecond == third);
                        }
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        } else if(!spinning){
            showText(context.getResources().getString(R.string.msg_broke));
        }
    }

    private void endSpin(boolean successful) {
        spinning = false;
        if(successful) {
            streak++;
            score += 100 * streak;
            if(streak > 1) showText(context.getResources().getString(R.string.string_streak) + ": "
                    + streak + "-x!!");
            showScore();
        } else streak = 0;
    }

    private void setPicture(int n, int num) {
        getViewState().setSlot(images.get(n), num);
    }

    private int dpToPx(int dp) {
        int den = context.getResources().getDisplayMetrics().densityDpi / 160;
        return dp * den;
    }

    private void showText(String text) {
        executor.execute(() -> handler.post(() -> getViewState().showMessage(text)));
    }

    private void showScore() {
        executor.execute(() -> handler.post(() -> getViewState().changeScore(score)));
    }

}
