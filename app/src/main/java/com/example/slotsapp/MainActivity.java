package com.example.slotsapp;

import androidx.constraintlayout.widget.ConstraintLayout;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.slotsapp.mvp.SlotPresenter;
import com.example.slotsapp.mvp.SlotPresenterViewInterface;
import com.example.slotsapp.util.Util;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;


import moxy.MvpAppCompatActivity;
import moxy.presenter.InjectPresenter;

public class MainActivity extends MvpAppCompatActivity implements SlotPresenterViewInterface {
    private Target target;

    @InjectPresenter
    SlotPresenter mPresenter;

    @Override
    public void onBackPressed() { finish(); }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setBackground();
        fullscreen();
        mPresenter.initialise(this);

        androidx.appcompat.widget.AppCompatButton button = findViewById(R.id.button_play);
        button.setOnClickListener(v -> mPresenter.spin());
    }

    @Override
    public void setSlot(Bitmap image, int num) {
        ImageView slot = null;
        ConstraintLayout parent = null;
        switch (num) {
            case 1 :
                slot = findViewById(R.id.imageView_slot_one);
                parent = findViewById(R.id.layout_slot_one_parent);
                break;
            case 2 :
                slot = findViewById(R.id.imageView_slot_two);
                parent = findViewById(R.id.layout_slot_two_parent);
                break;
            case 3 :
                slot = findViewById(R.id.imageView_slot_three);
                parent = findViewById(R.id.layout_slot_three_parent);
                break;
        }
        if(slot != null) Util.imageViewAnimatedChange(this, slot, parent, image);
    }

    @Override
    public void changeScore(int score) {
        TextView scoreView = findViewById(R.id.textView_score);
        scoreView.setText(String.valueOf(score));
    }

    @Override
    public void showMessage(String message) {
        LinearLayout ll = findViewById(R.id.layout_msg);
        TextView messageView = findViewById(R.id.textView_msg);
        messageView.setText(message);
        if(message.length() > 10) messageView.setTextSize(20);
        else messageView.setTextSize(38);

        ll.setVisibility(View.VISIBLE);

        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(() -> ll.setVisibility(View.GONE), 2000);
    }

    private void fullscreen() {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }

    private void setBackground() {
        ConstraintLayout back = findViewById(R.id.layout_main);
        target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                back.setBackground(new BitmapDrawable(getResources(), bitmap));
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };
        Picasso.get().load("http://195.201.125.8/SlotsApp/background.png").into(target);
    }
}